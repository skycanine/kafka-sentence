#!/bin/bash
#

set -e
#set -ex

: ${image_this:="tropicalcanine/kafka"}
: ${tag:="build-latest"}

docker build -t ${image_this}:${tag} src/main/docker

echo -e "\n============ Now test authentication and authorization."
$(dirname $0)/src/test/bin/kafka-authentication-test.sh
echo -e "\n============ Now test compose scaling of kafka."
$(dirname $0)/src/test/bin/kafka-dc-scale-test.sh
