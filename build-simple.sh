#!/bin/bash
#
# Very simple. No SSL; no verified tests
#

set -e
#set -ex

: ${NETWORK_NAME_KAFKA:="kafkanet"}
: ${IMAGE_KAFKA:="tropicalcanine/kafka:build-latest"}
NOW_HUMAN="$(echo $(date))"

#########################
#
#
createDockerNetworkKafka() {
	echo "==== Create a kafka docker network."
	docker network create $NETWORK_NAME_KAFKA || echo "Ignore already exists"
}

docker build -t ${IMAGE_KAFKA} src/main/docker


createDockerNetworkKafka

docker stop zookeeper kafka || echo "Ignore if not there"
docker rm -v zookeeper kafka || echo "Ignore if not there"

docker run --name zookeeper -d -p 2181:2181 --net "$NETWORK_NAME_KAFKA"  "$IMAGE_KAFKA" bin/zookeeper-server-start.sh config/zookeeper.properties

echo Waiting
sleep 5 # TODO look in logs instead

docker run --name kafka -d -p 9092:9092 -p 9093:9093 --net "$NETWORK_NAME_KAFKA"  "$IMAGE_KAFKA" bin/kafka-server-start.sh config/server.properties

echo Waiting
sleep 5 # TODO look in logs instead

docker exec -it kafka bin/kafka-topics.sh --create --zookeeper zookeeper:2181 --replication-factor 1 --partitions 1 --topic test

docker exec -it kafka bin/kafka-topics.sh --list --zookeeper zookeeper:2181

echo "A message here" | docker exec -i kafka bin/kafka-console-producer.sh --broker-list kafka:9092 --topic test

docker exec -i kafka bin/kafka-console-consumer.sh \
		--bootstrap-server kafka:9092 \
		--topic test \
		--max-messages 1 \
		--from-beginning \
		--timeout-ms 3000


