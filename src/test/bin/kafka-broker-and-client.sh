#!/bin/bash
#
# Functions for Kafka brokers, Zookeepers, and Kafka clients, both producers and consumers.
#

: ${TAG_ZOOKEEPER:="3.4"}

: ${PORT_ZOOKEEPER_CLIENT:="2181"}
: ${PORT_ZOOKEEPER_FOLLOWER:="2188"}
: ${PORT_ZOOKEEPER_ELECTION:="3888"}

: ${ZOO_SERVER_NAME:="zookeeper"}
: ${NETWORK_NAME_KAFKA:="kafkanet"}
: ${IMAGE_KAFKA:="tropicalcanine/kafka:build-latest"}
: ${CERTS_DIR:="/var/private/ssl"}
: ${KEY_PASS:="111111"}
NOW_HUMAN="$(echo $(date))"

#########################
#
#
createDockerNetworkKafka() {
	echo "==== Create a kafka docker network."
	docker network create $NETWORK_NAME_KAFKA || echo "Ignore already exists"
}

#########################
#
#
startZookeeperSingle() {

	echo "==== Start a Zookeeper."

	#docker run --name $ZOO_SERVER_NAME --net $NETWORK_NAME_KAFKA --restart=always --env ZOO_MY_ID=1 --env ZOO_SERVERS="server.1=${ZOO_SERVER_NAME}:2888:3888" -d zookeeper

	docker run --name $ZOO_SERVER_NAME --net $NETWORK_NAME_KAFKA -d zookeeper:${TAG_ZOOKEEPER}

	# Test it
	docker run --rm -it --net $NETWORK_NAME_KAFKA zookeeper:${TAG_ZOOKEEPER} zkCli.sh -server $ZOO_SERVER_NAME delete /zktest
	docker run --rm -it --net $NETWORK_NAME_KAFKA zookeeper:${TAG_ZOOKEEPER} zkCli.sh -server $ZOO_SERVER_NAME create /zktest "$NOW_HUMAN"
	docker run --rm -it --net $NETWORK_NAME_KAFKA zookeeper:${TAG_ZOOKEEPER} zkCli.sh -server $ZOO_SERVER_NAME get /zktest | grep "$NOW_HUMAN"

}

#########################
#
# Default is false - By default, if a Resource R has no associated ACLs, no one other than super users are allowed to access R.
#	allow.everyone.if.no.acl.found=false
#
#
#
startKafkaSingle() {
	local keystoreFileIndex=${1:-1}
	local keystoreFilePrefix=${2:-server}
	local useACLAuthorizer=${3:-useACLAuthorizer}

# Setting the super user properties below via command line has equals escape issues which I have avoided by putting them into the server.properties file via the Dockerfile.
#--override super.users='User:kafkasuperclient;User:CN=kafkabroker'
	local aclProperties="
--override authorizer.class.name=kafka.security.auth.SimpleAclAuthorizer
"
	echo "==== Start a Kafka; use ACL: $useACLAuthorizer."

	[ "$useACLAuthorizer" == "useACLAuthorizer" ] || aclProperties=""


	docker run -d --name kafka --net $NETWORK_NAME_KAFKA -v certs:$CERTS_DIR -p 9092:9092 -p 9093:9093 $IMAGE_KAFKA \
		bin/kafka-server-start.sh config/server.properties \
			--override listeners=PLAINTEXT://:9092,SSL://:9093 \
			--override ssl.keystore.location=$CERTS_DIR/${keystoreFilePrefix}.${keystoreFileIndex}.keystore.jks \
			--override ssl.keystore.password=$STORE_PASS \
			--override ssl.key.password=$KEY_PASS \
			--override ssl.truststore.location=$CERTS_DIR/${keystoreFilePrefix}.truststore.jks \
			--override ssl.truststore.password=$STORE_PASS \
			--override ssl.secure.random.implementation=SHA1PRNG \
			--override security.inter.broker.protocol=SSL \
			--override ssl.client.auth=required \
			$aclProperties

			# --override ssl.endpoint.identification.algorithm=HTTPS # perform hostname verification using Common Name (CN) or Subject Alternative Name (SAN)
			# --override zookeeper.connect=zookeeper:2181,zookeeper2:2181
			# ssl.client.auth=none OR "required" => client authentication is required with certs
			# Inter-broker communication defaults to PLAINTEXT

	# Check the log for SSL.  TODO Fail if not there for a number of iterations.
	# Kafka takes about half a second to start.
	while ! docker logs kafka |grep SSL; do sleep 1; done

	# Test the server keystore and truststore setup.  
	# NOTE: the openssl in nginx:latest does not show the cert BEGIN/END, but openssl installed in alpine does show it.
	# SEE http://stackoverflow.com/questions/17203562/openssl-s-client-cert-proving-a-client-certificate-was-sent-to-the-server
	# This line has output that shows it sent 12 bytes to the server as (assumed) a "no client certificate" message.
	docker run -it --rm --net $NETWORK_NAME_KAFKA -v certs:$CERTS_DIR --env KEY_PASS="$KEY_PASS" $IMAGE_KAFKA \
		bash -c "echo | timeout 2s openssl s_client -debug -state -connect kafka:9093 -tls1 2>&1" |grep "END CERTIFICATE"

	# Below the output will include the binary form of your client certificate.  BUT it needs a password.
	# SEE https://wiki.openssl.org/index.php/Manual:Openssl(1)#PASS_PHRASE_ARGUMENTS
	# But we get the below response:
	# 	unable to load client certificate private key file
	# 	140349011676044:error:0907B068:PEM routines:PEM_READ_BIO_PRIVATEKEY:bad password read:pem_pkey.c:117:
	#docker run -it --rm --net $NETWORK_NAME_KAFKA -v certs:$CERTS_DIR --env KEY_PASS="$KEY_PASS" $IMAGE_KAFKA \
	#	bash -c "echo | timeout 2s openssl s_client -debug -state -connect kafka:9093 -tls1 -cert $CERTS_DIR/ca-cert -key $CERTS_DIR/ca-key -pass pass:$KEYPASS 2>&1" |grep "END CERTIFICATE"

}

#########################
#
# Default is false - By default, if a Resource R has no associated ACLs, no one other than super users are allowed to access R.
#	allow.everyone.if.no.acl.found=false
#
#	Below may or may not be necessary depending on if we have a shared volume among all brokers.
#			--override log.dirs=/tmp/kafka-logs-$keystoreFileIndex
#
# TODO scale giving containers the same name; each IP will be different and the proxy will be aware of that.
#
startKafkaMultipleBroker() {
	local keystoreFileIndex=${1:-1}
	local keystoreFilePrefix=${2:-server}
	local useACLAuthorizer=${3:-useACLAuthorizer}
	local containerName=${4:-kafka}
	local kafkaPLAINTEXTPort=${5:-9092}
	local kafkaSSLPort=${6:-9093}

# Setting the super user properties below via command line has equals escape issues which I have avoided by putting them into the server.properties file via the Dockerfile.
#--override super.users='User:kafkasuperclient;User:CN=kafkabroker'
	local aclProperties="
--override authorizer.class.name=kafka.security.auth.SimpleAclAuthorizer
"
	echo "==== Start a Kafka; use ACL: $useACLAuthorizer."

	[ "$useACLAuthorizer" == "useACLAuthorizer" ] || aclProperties=""

	docker run -d --name $containerName --net $NETWORK_NAME_KAFKA -v certs:$CERTS_DIR $IMAGE_KAFKA \
		bin/kafka-server-start.sh config/server.properties \
			--override listeners=PLAINTEXT://:${kafkaPLAINTEXTPort},SSL://:${kafkaSSLPort} \
			--override ssl.keystore.location=$CERTS_DIR/${keystoreFilePrefix}.${keystoreFileIndex}.keystore.jks \
			--override ssl.keystore.password=$STORE_PASS \
			--override ssl.key.password=$KEY_PASS \
			--override ssl.truststore.location=$CERTS_DIR/${keystoreFilePrefix}.truststore.jks \
			--override ssl.truststore.password=$STORE_PASS \
			--override ssl.secure.random.implementation=SHA1PRNG \
			--override security.inter.broker.protocol=SSL \
			--override ssl.client.auth=required \
			--override broker.id=${keystoreFileIndex} \
			$aclProperties

			# --override ssl.endpoint.identification.algorithm=HTTPS # perform hostname verification using Common Name (CN) or Subject Alternative Name (SAN)
			# --override zookeeper.connect=zookeeper:2181,zookeeper2:2181
			# ssl.client.auth=none OR "required" => client authentication is required with certs
			# Inter-broker communication defaults to PLAINTEXT

	# Check the log for SSL.  TODO Fail if not there for a number of iterations.
	# Kafka takes about half a second to start.
	while ! docker logs kafka |grep SSL; do sleep 1; done

	# Test the server keystore and truststore setup.  
	# NOTE: the openssl in nginx:latest does not show the cert BEGIN/END, but openssl installed in alpine does show it.
	# SEE http://stackoverflow.com/questions/17203562/openssl-s-client-cert-proving-a-client-certificate-was-sent-to-the-server
	# This line has output that shows it sent 12 bytes to the server as (assumed) a "no client certificate" message.
	docker run -it --rm --net $NETWORK_NAME_KAFKA -v certs:$CERTS_DIR --env KEY_PASS="$KEY_PASS" $IMAGE_KAFKA \
		bash -c "echo | timeout 2s openssl s_client -debug -state -connect kafka:$kafkaSSLPort -tls1 2>&1" |grep "END CERTIFICATE"

	# Below the output will include the binary form of your client certificate.  BUT it needs a password.
	# SEE https://wiki.openssl.org/index.php/Manual:Openssl(1)#PASS_PHRASE_ARGUMENTS
	# But we get the below response:
	# 	unable to load client certificate private key file
	# 	140349011676044:error:0907B068:PEM routines:PEM_READ_BIO_PRIVATEKEY:bad password read:pem_pkey.c:117:
	#docker run -it --rm --net $NETWORK_NAME_KAFKA -v certs:$CERTS_DIR --env KEY_PASS="$KEY_PASS" $IMAGE_KAFKA \
	#	bash -c "echo | timeout 2s openssl s_client -debug -state -connect kafka:9093 -tls1 -cert $CERTS_DIR/ca-cert -key $CERTS_DIR/ca-key -pass pass:$KEYPASS 2>&1" |grep "END CERTIFICATE"

}



#########################
#
# Set the replication factor the the number of kafka brokers.
#
createTopic() {
	local topicName=${1:-test}
	local replicationFactor=${2:-1}
	local partitions=${3:-1}

	echo "==== Create a topic named $topicName."

	docker run -it --rm --net $NETWORK_NAME_KAFKA $IMAGE_KAFKA bin/kafka-topics.sh --create --zookeeper ${ZOO_SERVER_NAME}:2181 --replication-factor $replicationFactor --partitions $partitions --topic $topicName
	docker run -it --rm --net $NETWORK_NAME_KAFKA $IMAGE_KAFKA bin/kafka-topics.sh --list --zookeeper ${ZOO_SERVER_NAME}:2181 |grep "$topicName"
	# --zookeeper zoo1:2181,zoo2,2181
}


#########################
#
# Observe which brokers are listening on the topic
#
describeTopic() {
	local topicName=${1:-test}

	docker run -it --rm --net $NETWORK_NAME_KAFKA $IMAGE_KAFKA bin/kafka-topics.sh --describe --zookeeper ${ZOO_SERVER_NAME}:2181 --topic $topicName
}


#########################
#
#
# Each stdin line is a separate message. Otherwise use --line-reader <reader_class>.
#
# client.id - An id string to pass to the server when making requests. The purpose of this is to be able to track the source of requests beyond just ip/port by allowing a logical application name to be included in server-side request logging.
#
# security.protocol - Protocol used to communicate with brokers. Valid values are: PLAINTEXT, SSL, SASL_PLAINTEXT, SASL_SSL.
#
#
produceToTopic() {
	local keystoreFileIndex=${1:-1}
	local keystoreFilePrefix=${2:-client}
	local kafkaPort=${3:-9093}
	local topicName=${4:-test}
	local maxBlockMS=${5:-1000}

	echo "==== Produce to topic $topicName with $keystoreFilePrefix $keystoreFileIndex."

	! docker run -it --rm --name kafkaproducer --net $NETWORK_NAME_KAFKA -v certs:$CERTS_DIR $IMAGE_KAFKA bash -c "echo $NOW_HUMAN | bin/kafka-console-producer.sh --broker-list kafka:$kafkaPort --topic $topicName \
	--max-block-ms=$maxBlockMS \
	--producer-property ssl.key.password=$KEY_PASS \
	--producer-property ssl.keystore.location=$CERTS_DIR/${keystoreFilePrefix}.${keystoreFileIndex}.keystore.jks \
	--producer-property ssl.keystore.password=$STORE_PASS \
	--producer-property ssl.truststore.location=$CERTS_DIR/client.truststore.jks \
	--producer-property ssl.truststore.password=$STORE_PASS \
	--producer-property client.id=producer.${keystoreFileIndex} \
	--producer-property security.protocol=SSL" \
	| tee /dev/tty | grep "ERROR" || return 1

#	--producer-property ssl.enabled.protocols=TLSv1.2,TLSv1.1,TLSv1 \
#	--producer-property ssl.keystore.type=JKS \
#	--producer-property ssl.truststore.type=JKS \
#	--producer-property ssl.secure.random.implementation=SHA1PRNG

	# --producer.config client-ssl.properties
	# TODO --broker-list kafka:9093,kafka2:9093


}

#########################
#
#
consumeFromTopic() {
	local keystoreFileIndex=${1:-1}
	local keystoreFilePrefix=${2:-client}
	local topicName=${3:-test}

	echo "==== Consume from topic $topicName with $keystoreFilePrefix $keystoreFileIndex."

	# Below is continual.
	# docker run -it --rm --name kafkaconsumer --net $NETWORK_NAME_KAFKA $IMAGE_KAFKA bin/kafka-console-consumer.sh --bootstrap-server kafka:9092 --topic $topicName --from-beginning
	#
	# --consumer-property consumer.timeout.ms=5000
	# --offset <consume offset>               
	# The offset id to consume from (a non-negative number), or 'earliest' which means from beginning, or 'latest' which means from end (default: latest)        
	# --consumer.config config/consumer.properties
	# consumer.properties:
	#	group.id=myConsumerGroup
	#	# Timeout so the consumer stops waiting
	#	consumer.timeout.ms=5000
	#
	# DOESN'T WORK: bin/kafka-console-consumer.sh --bootstrap-server kafka:9092 --topic test --consumer-property consumer.timeout.ms=5000 --from-beginning

	# Consume one message from the beginning and verify
	# PLAINTEXT
	# When using ACLs on PLAINTEXT we see a GroupAuthorizationException: Not authorized to access group: console-consumer-73893
	# echo "Consuming with PLAINTEXT..."
	# docker run -it --rm --name kafkaconsumer --net $NETWORK_NAME_KAFKA $IMAGE_KAFKA bin/kafka-console-consumer.sh --bootstrap-server kafka:9092 --topic $topicName --max-messages 1 --from-beginning | grep "${NOW_HUMAN}"

	 # minimum SSL properties
	 CONSUMER_SSL_PROPERTIES="\
	--consumer-property security.protocol=SSL \
	--consumer-property ssl.truststore.location=$CERTS_DIR/client.truststore.jks \
	--consumer-property ssl.truststore.password=$STORE_PASS \
	"

	# when client authentication is "required" and use client.keystore.jks
	CONSUMER_AUTH_REQUIRED_PROPERTIES="\
	--consumer-property ssl.keystore.location=$CERTS_DIR/${keystoreFilePrefix}.${keystoreFileIndex}.keystore.jks \
	--consumer-property ssl.keystore.password=$STORE_PASS \
	--consumer-property ssl.key.password=$KEY_PASS \
	"

#	--consumer-property ssl.enabled.protocols=TLSv1.2,TLSv1.1,TLSv1 \
#	--consumer-property ssl.keystore.type=JKS \
#	--consumer-property ssl.truststore.type=JKS \
#

     # --consumer.config client-ssl.properties
	 # --partition 1

	# SSL
	echo "Consuming with SSL..."
	# TODO really, max-messages should be the number of messages in the queue; otherwise we are just getting the first message (which may be OK if the script only makes one message for this topic).
	docker run -it --rm --name kafkaconsumer --net $NETWORK_NAME_KAFKA -v certs:$CERTS_DIR $IMAGE_KAFKA bin/kafka-console-consumer.sh \
		--bootstrap-server kafka:9093 \
		--topic $topicName \
		--max-messages 1 \
		--from-beginning \
		--timeout-ms 3000 \
		$CONSUMER_SSL_PROPERTIES \
		$CONSUMER_AUTH_REQUIRED_PROPERTIES \
		| tee /dev/tty | grep "$NOW_HUMAN"

	# Verify zookeeper created the consumer group
	#docker run --rm -it --net $NETWORK_NAME_KAFKA zookeeper:${TAG_ZOOKEEPER} zkCli.sh -server $ZOO_SERVER_NAME ls /consumers | grep "myConsumerGroup"
	#docker run --rm -it --net $NETWORK_NAME_KAFKA zookeeper:${TAG_ZOOKEEPER} zkCli.sh -server $ZOO_SERVER_NAME get /consumers/myConsumerGroup/offsets/$topicName/0
}


#########################
#
#
resetKafka() {
	echo "==== Reset kafka, zookeeper, and the certificates."
	docker stop kafka zookeeper || echo "Kafka/Zookeepker containers already stopped"
	docker rm -v kafka zookeeper || echo "Kafka/Zookeepker images already removed"
	docker volume ls | awk '{print $2}' |grep "^certs$" && docker volume rm certs
	#docker volume rm certs || echo "Kafka/Zookeeper volumes already removed"
}
