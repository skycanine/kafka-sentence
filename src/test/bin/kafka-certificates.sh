#!/bin/bash
#
# Functions to create Kafka certificates.
#

: ${NETWORK_NAME_KAFKA:="kafkanet"}
: ${IMAGE_KAFKA:="tropicalcanine/kafka:build-latest"}
: ${CERTS_DIR:="/var/private/ssl"}
: ${KEY_PASS:="111111"}
: ${STORE_PASS:="222222"}

#########################
#
# Common Name - CN, hostname, first and last name
# Distinguished Name - DN, -dname
#
generateAKeystore() {
	local keystoreFileIndex=${1:-1}
	local keystoreFilePrefix=${2:-server}
	local commonName=${3:-kafkabroker}
	local validityDays=${4:-90}
	local alias=${5:-localhost}

	echo "==== Generate a keystore for $keystoreFilePrefix $keystoreFileIndex with CN=$commonName."

	docker run -it --rm --net $NETWORK_NAME_KAFKA -v certs:$CERTS_DIR $IMAGE_KAFKA \
		keytool \
		-genkey \
		-noprompt \
		-alias "$alias" \
		-keystore $CERTS_DIR/${keystoreFilePrefix}.${keystoreFileIndex}.keystore.jks \
		-validity $validityDays \
		-keyalg RSA \
		-keysize 2048 \
		-storepass $STORE_PASS \
		-keypass $KEY_PASS \
		-dname "CN=$commonName"
#		-dname "CN=$commonName,OU=Unknown,O=Unknown,L=Unknown,ST=Unknown,C=Unknown"
#		-dname "CN=$commonName, OU=SaaS, O=CA Tech, L=Cary, ST=North Carolina, C=NC"

	
	# Verify the Common Name (CN) is in the keystore
	docker run -it --rm --net $NETWORK_NAME_KAFKA -v certs:$CERTS_DIR $IMAGE_KAFKA \
		keytool \
		-list -v \
		-keystore $CERTS_DIR/${keystoreFilePrefix}.${keystoreFileIndex}.keystore.jks \
		-alias "$alias" \
		-storepass $STORE_PASS \
		| grep "Issuer" |grep "$commonName"
}


#########################
#
# SEE https://www.openssl.org/docs/man1.0.2/apps/req.html
#
generateCertificateAuthority() {
	local caKeyFile=${1:ca-key}
	local caCertFile=${2:ca-cert}
	local validityDays=${3:-90}
	# below needs debian package sharutils or use package pwgen (pwgen -c -n -1 12)
	# local pass=$(head -c 128 /dev/random  | uuencode - | grep -v "^end" | tr "\n" "d")
	local subj="
C=US
ST=NC
O=Wolfe Software
localityName=Cary
commonName=wolfesoftware.com
organizationalUnitName=SaaS
emailAddress=
"
# C=<Country Code>
# ST=<State>
# O=<Company>
# localityName=<City>
# commonName=$DOMAIN
# organizationalUnitName=<Unit Name>
# emailAddress=<email>

	echo "==== Generate a certificate authority (CA)."
	docker run -it --rm -v certs:$CERTS_DIR --env KEY_PASS="$KEY_PASS" $IMAGE_KAFKA \
		openssl req \
		-new \
		-x509 \
		-keyout $CERTS_DIR/$caKeyFile \
		-out $CERTS_DIR/$caCertFile \
		-days $validityDays \
		-passout env:KEY_PASS \
		-batch \
		-subj "$(echo -n "$subj" | tr "\n" "/")"
}


#########################
#
# Add the Certificate Authority to the client or server trust store
# clients (or servers) can trust this CA.
# The client truststore stores all the certificates that the client should trust.
#
addCAToTrustStore() {
	local truststoreFilePrefix=${1:-server}
	local caCertFile=${2:ca-cert}
	local caRootAlias=${3:-CARoot}

	echo "==== Add the CA to a truststore for $truststoreFilePrefix."
	docker run -it --rm --net $NETWORK_NAME_KAFKA -v certs:$CERTS_DIR $IMAGE_KAFKA \
		keytool \
		-import \
		-keystore $CERTS_DIR/$truststoreFilePrefix.truststore.jks \
		-noprompt \
		-alias "$caRootAlias" \
		-storepass $STORE_PASS \
		-file $CERTS_DIR/$caCertFile
}


#########################
#
#
verifyCAAddedToTrustStore() {
	local truststoreFilePrefix=${1:-server}
	local caRootAlias=${2:-CARoot}

	# ("set -ex" on the script will stop on error)
	echo "==== Verify the truststore for $truststoreFilePrefix has the CA."
	docker run -it --rm --net $NETWORK_NAME_KAFKA -v certs:$CERTS_DIR $IMAGE_KAFKA \
		keytool \
		-list -v \
		-keystore $CERTS_DIR/$truststoreFilePrefix.truststore.jks \
		-alias "$caRootAlias" \
		-storepass $STORE_PASS \
		>/dev/null 2>&1

	return $?
}

#########################
#
#
exportSignAndImportCert() {
	local keystoreFileIndex=${1:-1}
	local keystoreFilePrefix=${2:-server}
	local caRootAlias=${3:-CARoot}
	local alias=${4:-localhost}
	local caKeyFile=${5:ca-key}
	local caCertFile=${6:ca-cert}
	local validityDays=${7:-90}

	# Export the kafka cert from the keystore. For each kafka.
	echo "==== Export, sign and import the certificate for $keystoreFilePrefix $keystoreFileIndex."
	docker run -it --rm --net $NETWORK_NAME_KAFKA -v certs:$CERTS_DIR $IMAGE_KAFKA \
		keytool \
		-certreq \
		-keystore $CERTS_DIR/${keystoreFilePrefix}.${keystoreFileIndex}.keystore.jks \
		-noprompt \
		-alias "$alias" \
		-storepass $STORE_PASS \
		-keypass $KEY_PASS \
		-file $CERTS_DIR/cert-file

	# Then sign the kafka cert with the CA.  For each kafka.
	docker run -it --rm -v certs:$CERTS_DIR --env KEY_PASS="$KEY_PASS" $IMAGE_KAFKA \
		openssl x509 \
		-req \
		-CA $CERTS_DIR/$caCertFile \
		-CAkey $CERTS_DIR/$caKeyFile \
		-in $CERTS_DIR/cert-file \
		-out $CERTS_DIR/cert-signed \
		-days $validityDays \
		-CAcreateserial \
		-passin env:KEY_PASS

	# Import both the certificate of the CA and the signed certificate into the server keystore.
	docker run -it --rm --net $NETWORK_NAME_KAFKA -v certs:$CERTS_DIR $IMAGE_KAFKA \
		keytool \
		-import \
		-keystore $CERTS_DIR/${keystoreFilePrefix}.${keystoreFileIndex}.keystore.jks \
		-noprompt \
		-alias "$caRootAlias" \
		-storepass $STORE_PASS \
		-file $CERTS_DIR/$caCertFile

	docker run -it --rm --net $NETWORK_NAME_KAFKA -v certs:$CERTS_DIR $IMAGE_KAFKA \
		keytool \
		-import \
		-keystore $CERTS_DIR/${keystoreFilePrefix}.${keystoreFileIndex}.keystore.jks \
		-noprompt \
		-alias "$alias" \
		-storepass $STORE_PASS \
		-keypass $KEY_PASS \
		-file $CERTS_DIR/cert-signed
}

#########################
#
# The certificate files are:
#	ca-cert: the certificate of the CA
#	ca-key: the private key of the CA
#	cert-file: the exported, unsigned certificate of the server
#	cert-signed: the signed certificate of the server
#	*.jks: java key stores
#
listCertificateFiles() {
	echo "==== See the certificate files below."
	docker run -it --rm --net $NETWORK_NAME_KAFKA -v certs:$CERTS_DIR $IMAGE_KAFKA ls -ltrh $CERTS_DIR
}

# generateAKafkaBrokerKeystore
#########################
#
#
generateSSLKeyAndCertForKafkaBrokerAndClientAndCA() {

	docker volume create certs

	# *** 1. Run below for each kafka broker in the cluster.

	# Generate a kafka broker keystore (server)
	generateAKeystore 1 "server" "kafkabroker"

	# Generate a kafka broker keystore (server)
	generateAKeystore 2 "server" "kafkabroker"

	# Generate a kafka broker keystore (server)
	generateAKeystore 3 "server" "kafkabroker"

	# Generate a kafka broker keystore (client)
	generateAKeystore 0 "client" "kafkasuperclient"

	# Generate another kafka broker keystore (client)
	generateAKeystore 1 "client" "kafkaclientone"

	# Generate another kafka broker keystore (client)
	generateAKeystore 2 "client" "kafkaclienttwo"


	# *** 2. Generate a cert to use as a Certificate Authority (CA) for all.
	# Alternatively, use an another image that has openssl.
	generateCertificateAuthority "ca-key" "ca-cert"

	# Add the Certificate Authority to the client trust store (clients can trust this CA).
	# The client truststore stores all the certificates that the client should trust.
	addCAToTrustStore "client" "ca-cert" "CARoot"

	# Add the Certificate Authority to the server trust store (servers can trust this CA).
	# Truststore for the kafka brokers since ssl.client.auth=required
	addCAToTrustStore "server" "ca-cert" "CARoot"

	# Verify the CA was added to the truststore for the kafka broker (server)
	verifyCAAddedToTrustStore "server" "CARoot"


	# *** 3. Sign the kafka broker certificates with the Certificate Authority
	exportSignAndImportCert 1 "server" "CARoot" "localhost" "ca-key" "ca-cert"
	exportSignAndImportCert 2 "server" "CARoot" "localhost" "ca-key" "ca-cert"
	exportSignAndImportCert 3 "server" "CARoot" "localhost" "ca-key" "ca-cert"

	# Do the same for the client keystore(s)
	exportSignAndImportCert 0 "client" "CARoot" "localhost" "ca-key" "ca-cert"
	exportSignAndImportCert 1 "client" "CARoot" "localhost" "ca-key" "ca-cert"
	exportSignAndImportCert 2 "client" "CARoot" "localhost" "ca-key" "ca-cert"

	listCertificateFiles
}
