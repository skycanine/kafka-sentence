#!/bin/bash
#
# Functions for Kafka Authorization.
#

: ${NETWORK_NAME_KAFKA:="kafkanet"}
: ${IMAGE_KAFKA:="tropicalcanine/kafka:build-latest"}
: ${CERTS_DIR:="/var/private/ssl"}

#########################
#
#
listACLs() {
	local topicName=${1:-test}

	echo "==== List the ACLs for topic $topicName."
	docker run -it --rm  --net $NETWORK_NAME_KAFKA -v certs:$CERTS_DIR $IMAGE_KAFKA bin/kafka-acls.sh \
		--authorizer-properties zookeeper.connect=zookeeper:2181 \
		--list \
		--topic $topicName
}


#########################
#
# --operation {Read, Write, Create, Delete, Alter, Describe, ClusterAction, All}
#		--allow-principal User:barney \
#		--producer
#		--consumer \
#		--group consumergroup-1
#		--allow-host 192.168.1.0 \
#		--allow-host 192.168.1.1 \
#	Consumer Group to which the ACLs should be added or removed.  
#	A value of * indicates the ACLs should apply to all groups.
#		--group "*"
#
# Output:
# User:fred has Allow permission for operations: Read from hosts: *
# User:fred has Allow permission for operations: Write from hosts: * 
#
# Consumer needs: READ, DESCRIBE on topic and READ on group
# Producer needs: WRITE, DESCRIBE on topic and CREATE on cluster.                         
#
# Below has an unwanted consumer-group ACL restriction.
#	docker run -it --rm  --net $NETWORK_NAME_KAFKA -v certs:$CERTS_DIR $IMAGE_KAFKA bin/kafka-acls.sh \
#		--authorizer-properties zookeeper.connect=zookeeper:2181 \
#		--add \
#		--topic $topicName \
#		--operation Read \
#		--operation Write \
#		--allow-principal "User:CN=${principal}"
#
# Try to get the right operations but not right.
#	docker run -it --rm  --net $NETWORK_NAME_KAFKA -v certs:$CERTS_DIR $IMAGE_KAFKA bin/kafka-acls.sh \
#		--authorizer-properties zookeeper.connect=zookeeper:2181 \
#		--add \
#		--topic $topicName \
#		--operation Read \
#		--operation Write \
#		--operation Describe \
#		--allow-principal "User:CN=${principal}"
#
addACL() {
	local principal=${1:-kafkaclientone}
	local topicName=${2:-test}

	echo "==== Add ACLs for topic $topicName for $principal."
	# Convenience options
	docker run -it --rm  --net $NETWORK_NAME_KAFKA -v certs:$CERTS_DIR $IMAGE_KAFKA bin/kafka-acls.sh \
		--authorizer-properties zookeeper.connect=zookeeper:2181 \
		--add \
		--topic $topicName \
		--producer \
		--allow-principal "User:CN=${principal}"
	docker run -it --rm  --net $NETWORK_NAME_KAFKA -v certs:$CERTS_DIR $IMAGE_KAFKA bin/kafka-acls.sh \
		--authorizer-properties zookeeper.connect=zookeeper:2181 \
		--add \
		--topic $topicName \
		--consumer \
		--group "*" \
		--allow-principal "User:CN=${principal}"
}


