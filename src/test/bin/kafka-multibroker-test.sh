#!/bin/bash
#

set -e
#set -ex

source $(dirname $0)/kafka-broker-and-client.sh
source $(dirname $0)/kafka-certificates.sh
source $(dirname $0)/kafka-authorization.sh

#########################
#
#
kafkaMultiBrokerTest() {
	echo -e "\n======== BEGIN kafkaMultiBrokerTest()"
	createDockerNetworkKafka
	generateSSLKeyAndCertForKafkaBrokerAndClientAndCA
	startZookeeperSingle
	# FIXME in progress
	startKafkaMultipleBroker 1
	startKafkaMultipleBroker 2
	createTopic
	echo -e "\n======== Client 1 should pass after adding ACLs."
	addACL
	# FIXME ?need to adjust these functions?
	produceToTopic 1
	consumeFromTopic 1

	echo -e "\n======== END SUCCESS kafkaMultiBrokerTest()"
}

#####################
#
#####################

resetKafka
kafkaMultiBrokerTest
