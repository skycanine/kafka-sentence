#!/bin/bash
#

set -e
#set -ex

source $(dirname $0)/kafka-broker-and-client.sh
source $(dirname $0)/kafka-certificates.sh
source $(dirname $0)/kafka-authorization.sh

COMPOSE_DIR="$(dirname $0)"

#########################
#
# Bring up multiple kafka brokers with docker-compose scale.
#
# kafka talks to "zookeeper" which will resolve to one of the zookeepers (currently one).
#
# Host "kafka" in the network resolves to one of the running kafka brokers.
#
# The proxy could round robin the kafka hosts via:
#	resolver 127.0.0.11 valid=30s;
#	set $upstream_kafka http://kafka:9093;
#   	location /kafka/ {
#	proxy_pass              $upstream_kafka;
#	...
#
#
kafkaDCScaleTest() {
	echo -e "\n======== BEGIN kafkaDCScaleTest()"
	createDockerNetworkKafka

	echo -e "\n======== compose down"
	docker-compose --file $COMPOSE_DIR/docker-compose.yml --project-name kafka down

	resetKafka

	generateSSLKeyAndCertForKafkaBrokerAndClientAndCA
	echo -e "\n======== compose up"
	docker-compose --file $COMPOSE_DIR/docker-compose.yml --project-name kafka up -d
	echo -e "\n======== compose scale kafka=3"
	docker-compose --file $COMPOSE_DIR/docker-compose.yml --project-name kafka scale kafka=3

	echo -e "\n======== Observe the IPs for each kafka"
	docker network inspect kafkanet

	echo -e "\n======== ping kafka"
	docker run -it --rm --net kafkanet tropicalcanine/kafka:build-latest ping -c 1 kafka

	echo -e "\n======== nslookup kafka"
	docker run -it --rm --net kafkanet tropicalcanine/kafka:build-latest nslookup kafka

	echo -e "\n======== ping each kafka by name"
	docker run -it --rm --net kafkanet tropicalcanine/kafka:build-latest ping -c 1 kafka_kafka_1.kafkanet
	docker run -it --rm --net kafkanet tropicalcanine/kafka:build-latest ping -c 1 kafka_kafka_2.kafkanet

	echo -e "\n======== create topic, produce, consume"
	local topicName="test"
	local replicationFactor=3
	local partitions=6
	createTopic "$topicName" "$replicationFactor" "$partitions"
	produceToTopic 0
	consumeFromTopic 0

	echo -e "\n======== Observe which brokers are listening on the topic"
	describeTopic

	echo -e "\n======== END kafkaDCScaleTest()"
}

kafkaDCScaleTest
