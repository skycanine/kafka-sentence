#!/bin/bash
#
# Testing kafka authentication and authorization using SSL and ACLs.
#
# You can source the files for debugging without running this test like this using a unique approach ;)
#
#	source <(echo src/test/bin/kafka-*-test.sh src/test/bin/*.sh | tr ' ' '\n' |sort | uniq -u | while read fn; do echo source $fn; done)
#

#set -e
set -ex

source $(dirname $0)/kafka-broker-and-client.sh
source $(dirname $0)/kafka-certificates.sh
source $(dirname $0)/kafka-authorization.sh

COMPOSE_DIR="$(dirname $0)"

#########################
#
# @Ignore( "We need ACLs, so do not bother running this test." )
#
kafkaAuthenticationWithoutACLAuthorizerTest() {
	echo -e "\n======== BEGIN kafkaAuthenticationWithoutACLAuthorizerTest()"
	createDockerNetworkKafka
	generateSSLKeyAndCertForKafkaBrokerAndClientAndCA
	startZookeeperSingle
	startKafkaSingle 1 server doNotUseACLAuthorizer
	createTopic
	produceToTopic 1 client 9092
	consumeFromTopic
	echo "\n======== END SUCCESS kafkaAuthenticationWithoutACLAuthorizerTest()"
}

#########################
#
#
kafkaAuthenticationWithACLAuthorizerTest() {
	echo -e "\n======== BEGIN kafkaAuthenticationWithACLAuthorizerTest()"
	createDockerNetworkKafka
	generateSSLKeyAndCertForKafkaBrokerAndClientAndCA
	startZookeeperSingle
	startKafkaSingle
	createTopic
	echo -e "\n======== Produce and consume to topic \"test\" (default) as client super user 0"
	produceToTopic 0
	consumeFromTopic 0
	listACLs
	echo -e "\n======== Client 1 and 2 should FAIL since the ACLs are not yet there."
	! produceToTopic 1
	! consumeFromTopic 1
	! produceToTopic 2
	! consumeFromTopic 2
	echo -e "\n======== Client 1 should pass after adding ACLs."
	local topicName="test"
	local clientCN="kafkaclientone"
	addACL "$clientCN" "$topicName"
	produceToTopic 1
	consumeFromTopic 1
	echo -e "\n======== Client 2 producing and consuming should still FAIL."
	! produceToTopic 2
	! consumeFromTopic 2

	echo -e "\n======== Try another topic not ours.  Since the topic has no ACL for the client, producing and consuming should FAIL."
	local notMyTopicName="notMyTopic"
	createTopic "$notMyTopicName"
	! produceToTopic 1 client 9093 "$notMyTopicName"
	! consumeFromTopic 1 client "$notMyTopicName"

	echo -e "\n======== END SUCCESS kafkaAuthenticationTest()"
}

#####################
#
#####################

#resetKafka
#kafkaAuthenticationWithoutACLAuthorizerTest
docker-compose --file $COMPOSE_DIR/docker-compose.yml --project-name kafka down
resetKafka || echo "continuing after reset"
kafkaAuthenticationWithACLAuthorizerTest
