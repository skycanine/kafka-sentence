Kafka Sentence Docker
=====================


Build
-----

	./build.sh (with ACL and certs; needs adjusting to work again)

	OR for a simple kafka without ACL or tests

	./build-simple.sh (then stop here; you have a working kafka)

Following the build, the script runs tests that prove working kafka, zookeeper, authentication, authorization using SSL and ACLs.

SSL Authentication, Authorization and Tests
-------------------------------------------
SEE src/test/bin/kafka-authentication-test.sh


Kafka Broker Security
---------------------
Use SSL for authentication and authorization.  Create certificates for each server and for each client, both producers and consumers.  Use a trusted CA which signs all of them.

###### To consider:

- auto.create.topics.enable=true [Common cases](http://docs.confluent.io/2.0.0/kafka/authorization.html)
	SEE also [KafkaKAFKA-1674 auto.create.topics.enable docs are misleading](https://issues.apache.org/jira/browse/KAFKA-1674)

- principal.builder.class=CustomizedPrincipalBuilderClass

The default is apparently DefaultPrincipalBuilder; not used for SASL too according to https://github.com/apache/kafka/pull/1403 

	buildPrincipal(TransportLayer transportLayer, Authenticator authenticator)

could simply check the type of transportLayer and have different code paths for PLAINTEXT and SSL. At the moment, we would need to check if transportLayer is an instance of PlaintextTransportLayer or SslTransportLayer.

- offsets.storage=kafka

- dual.commit.enabled=true

- advertised.listeners=PLAINTEXT://10.....

- ssl.provider - The name of the security provider used for SSL connections. Default value is the default security provider of the JVM.

- ssl.endpoint.identification.algorithm - The endpoint identification algorithm to validate server hostname using server certificate.


ACLs
----
In the kafka broker config:
```
	super.users=User:CN=kafkabroker;User:CN=kafkasuperclient
```
The documentation says by default:
```
	"CN=writeuser,OU=Unknown,O=Unknown,L=Unknown,ST=Unknown,C=Unknown"
```

This broker config enables ACLs; otherwise all have all access.
```
	authorizer.class.name=kafka.security.auth.SimpleAclAuthorizer
```
To avoid having to set ACLs per topic for the servers:

make the server principal a super user; servers can automatically access all resources, including the cluster resource.


Additional Notes
----------------
- When using [HTTP over TLS](http://www.ietf.org/rfc/rfc2818.txt) a client is supposed 
to compare the CN portion of the subject Distinguished Name (DN) (The keytool -dname parameter)
in the server certificate to the DNS host name in the URL.  
Keytool calls CN "first and last name".

See Also
--------
- [KIP-11](https://cwiki.apache.org/confluence/display/KAFKA/KIP-11+-+Authorization+Interface)
- [CLI](https://cwiki.apache.org/confluence/display/KAFKA/Kafka+Authorization+Command+Line+Interface)
- [Security 7.4 Authorization and ACLs](http://kafka.apache.org/documentation.html#security)
- [Confluent Auth](http://docs.confluent.io/2.0.0/kafka/authorization.html)
- [openssl req](https://www.openssl.org/docs/man1.0.2/apps/req.html)
- [3.2 Producer Configs](http://kafka.apache.org/documentation/#producerconfigs)
- [openssl s_client OPTIONS -pass arg]( https://wiki.openssl.org/index.php/Manual:S_client(1) )
- [openssl s_client pass phrase arguments](https://wiki.openssl.org/index.php/Manual:Openssl(1)#PASS_PHRASE_ARGUMENTS)
- [Kafka Security 101](https://www.confluent.io/blog/apache-kafka-security-authorization-authentication-encryption/)

Learning Examples
-----------------
- #### Test Kafka
```
	: ${image_this:="tropicalcanine/kafka"}
	tag="build-latest"
	docker network create kafkanet
	docker run -d --name zookeeper --net kafkanet $image_this:$tag bin/zookeeper-server-start.sh config/zookeeper.properties
	# Watch the zookeeper log for the effect of the command below and entering random stdin text.

	docker run -it --rm --net kafkanet $image_this:$tag nc zookeeper 2181
	docker run -d --name kafka --net kafkanet -p 9092:9092 $image_this:$tag bin/kafka-server-start.sh config/server.properties
	docker run -it --rm --net kafkanet $image_this:$tag bin/kafka-topics.sh --create --zookeeper zookeeper:2181 --replication-factor 1 --partitions 1 --topic test
	docker run -it --rm --net kafkanet $image_this:$tag bin/kafka-topics.sh --list --zookeeper zookeeper:2181
	docker run -it --rm --name kafkaproducer --net kafkanet $image_this:$tag bin/kafka-console-producer.sh --broker-list kafka:9092 --topic test

	# type text and open another terminal
	docker run -it --rm --name kafkaconsumer --net kafkanet $image_this:$tag bin/kafka-console-consumer.sh --bootstrap-server kafka:9092 --topic test --from-beginning
```

- #### Kafka / Zookeeper in docker-compose (set up required)

Set up the certs, then:
```
	docker-compose --file src/test/bin/docker-compose.yml --project-name kafka down
	docker-compose --file src/test/bin/docker-compose.yml --project-name kafka up -d
	docker-compose --file src/test/bin/docker-compose.yml --project-name kafka scale kafka=3
	docker network inspect kafkanet
	docker run -it --rm --net kafkanet tropicalcanine/kafka:build-latest ping -c 1 kafka
	docker run -it --rm --net kafkanet tropicalcanine/kafka:build-latest nslookup kafka
	docker run -it --rm --net kafkanet tropicalcanine/kafka:build-latest ping -c 1 kafka_kafka_1.kafkanet
	docker run -it --rm --net kafkanet tropicalcanine/kafka:build-latest ping -c 1 kafka_kafka_2.kafkanet
```
If we did it this way, we would want to read values like the index from the environment.

The proxy could round robin the kafka hosts via:
```
	resolver 127.0.0.11 valid=30s;
	set $upstream_kafka http://kafka:9093;
   	location /kafka/ {
	proxy_pass              $upstream_kafka;
	...
```


Zookeeper
---------
[Zookeeper on Docker Hub](https://hub.docker.com/_/zookeeper/)

If not already created

	docker network create kafkanet

Start *Zookeeper*

	docker run --name zookeeper --net kafkanet -d zookeeper

Run a CLI on *Zookeeper*
```
	docker run --rm -it --net kafkanet zookeeper zkCli.sh -server zookeeper
	ls /
	create /zk "value"
	create /zk/node1 "nodeval"
	get /zk/node1
	set /zk/node1 "different"
```

*** here is config/zookeeper.properties of the zookeeper included in kafka
```
dataDir=/tmp/zookeeper
# the port at which the clients will connect
clientPort=2181
# disable the per-ip limit on the number of connections since this is a non-production config
maxClientCnxns=0
```

Here is a [GUI to Zookeeper](https://hub.docker.com/r/netflixoss/exhibitor/).



FAQ
---

#### PROBLEM:
	From client consumer:
```
	WARN Failed to send SSL Close message  (org.apache.kafka.common.network.SslTransportLayer)
java.io.IOException: Connection reset by peer
```
When the kafka broker starts with
```
		--override ssl.client.auth=none
```
and it's set to "none" instead of "required",
and the comsumer only has $CONSUMER_SSL_PROPERTIES, the "minimum SSL properties",
the consumer gets a message successfully.  That is, authentication looks like the problem.

Interbroker communication set to SSL probably doesn't come into play here since this test starts only one broker.

Kafka starts with a keystore and truststore.

??? Maybe I've confused the keystore and key passwords?  I could try setting them the same.

The non-auth client uses the client truststore with the store pass.  What did we put into the client truststore?

The non-working auth client uses the client keystore, client.keystore.jks.  Maybe the enabled protocols is not right?  What protocol did our openssl test use? "tls1"

What happens using the auth parms with the "none" kafka?

Note we can indeed produce via PLAINTEXT and process via SSL on the same topic.

We could try to use the server keystore in the auth client.  What happens?  It WORKS; make sure it's not because we're set to "none".  It still works set to "required".  SO, make the client.keystore.jks like the server.keystore.jks.  And then make sure we're using the CA propogated certs properly.

#### SOLUTION:
[By default, the user name associated with an SSL connection looks like the following. Often, people may want to extract one of the field (e.g., CN) as the user. It would be good if we can have a built-in principal builder that does that.](https://issues.apache.org/jira/browse/KAFKA-3532)

```
	CN=writeuser,OU=Unknown,O=Unknown,L=Unknown,ST=Unknown,C=Unknown
```

./clients/src/main/java/org/apache/kafka/common/network/SslTransportLayer.java

extends TransportLayer which is a parameter to 
```
	DefaultPrincipalBuilder.buildPrincipal(TransportLayer transportLayer, Authenticator authenticator)
		(java.security.Principal) return transportLayer.peerPrincipal();
```


###### Start the kafka broker while debugging:
```
	docker run --rm -it --name kafka --net $NETWORK_NAME_KAFKA -v certs:$CERTS_DIR -p 9092:9092 -p 9093:9093 $IMAGE_KAFKA bash

		bin/kafka-server-start.sh config/server.properties --override listeners=PLAINTEXT://:9092,SSL://:9093 --override ssl.keystore.location=/var/private/ssl/server.1.keystore.jks --override ssl.keystore.password=222222 --override ssl.key.password=111111 --override ssl.truststore.location=/var/private/ssl/server.truststore.jks --override ssl.truststore.password=222222 --override ssl.secure.random.implementation=SHA1PRNG --override security.inter.broker.protocol=SSL --override ssl.client.auth=required --override authorizer.class.name=kafka.security.auth.SimpleAclAuthorizer --override super.users='User:kafkabroker;User:kafkaclient2'
```
###### Ah hah
- When we change the generated keystores for client and server to use this shortened Distinguished Name
```
		-dname "CN=$commonName"
```
- and when we change the /opt/kafka/config/log4j.properties to do more debugging, matching the copy in experimental/src/test/, 
- and when we set into /opt/kafka/config/server.properties (the command line version fails with an equals sign in it):
```
	super.users=User:kafkabroker;User:kafkaclient2;User:CN=kafkabroker;User:CN=kafkaclient2
```
the kafka broker log shows the authorization using the super user list, with ACL on, is successful.
```
[2017-03-03 22:07:51,291] DEBUG principal = User:CN=kafkabroker is a super user, allowing operation without checking acls. (kafka.authorizer.logger)
[2017-03-03 22:07:51,291] DEBUG Principal = User:CN=kafkabroker is Allowed Operation = ClusterAction from host = 172.18.0.3 on resource = Cluster:kafka-cluster (kafka.authorizer.logger)
```

Now, a little further toward a working producer, we can see in the kafka broker log:
```
	[2017-03-03 22:53:10,510] DEBUG Processor 5 listening to new connection from /172.18.0.4:36924 (kafka.network.Processor)
	[2017-03-03 22:53:10,511] DEBUG SSLEngine.closeInBound() raised an exception. (org.apache.kafka.common.network.SslTransportLayer)
	javax.net.ssl.SSLException: Inbound closed before receiving peer's close_notify: possible truncation attack?
		at sun.security.ssl.Alerts.getSSLException(Alerts.java:208)
		at sun.security.ssl.SSLEngineImpl.fatal(SSLEngineImpl.java:1666)
        at sun.security.ssl.SSLEngineImpl.fatal(SSLEngineImpl.java:1634)
        at sun.security.ssl.SSLEngineImpl.closeInbound(SSLEngineImpl.java:1561)
        at org.apache.kafka.common.network.SslTransportLayer.handshakeFailure(SslTransportLayer.java:724)
        at org.apache.kafka.common.network.SslTransportLayer.handshake(SslTransportLayer.java:314)
        at org.apache.kafka.common.network.KafkaChannel.prepare(KafkaChannel.java:62)
        at org.apache.kafka.common.network.Selector.pollSelectionKeys(Selector.java:338)
        at org.apache.kafka.common.network.Selector.poll(Selector.java:291)
        at kafka.network.Processor.poll(SocketServer.scala:476)
        at kafka.network.Processor.run(SocketServer.scala:416)
        at java.lang.Thread.run(Thread.java:745)
	[2017-03-03 22:53:10,511] DEBUG Connection with kafkaproducer.kafkanet/172.18.0.4 disconnected (org.apache.kafka.common.network.Selector)
	javax.net.ssl.SSLException: Unrecognized SSL message, plaintext connection?
```
Ah, so the message is bad?  

When using the same docker container and using keyboard input stdin to give the message rather than from an echo:
```
	docker exec -it kafka bash
		bin/kafka-console-producer.sh --broker-list kafka:9093 --topic test --producer-property ssl.key.password=111111 --producer-property ssl.keystore.location=/var/private/ssl/client.2.keystore.jks --producer-property ssl.keystore.password=222222 --producer-property ssl.truststore.location=/var/private/ssl/client.truststore.jks --producer-property ssl.truststore.
password=222222 --producer-property client.id=producer.2 --producer-property security.protocol=SSL                                                                                       
asdfasdfasdf
```
		
the kafka log says, noting the acceptance of the super user client:
```
[2017-03-03 23:04:00,515] TRACE [KafkaApi-0] Handling request:{api_key=0,api_version=2,correlation_id=1,client_id=console-producer} -- {acks=1,timeout=1500,topic_data=[{topic=test,data=[{partition=0,record_set=java.nio.HeapByteBuffer[pos=0 lim=46 cap=46]}]}]} from connection 172.18.0.3:9093-172.18.0.3:33092;securityProtocol:SSL,principal:User:CN=kafkaclient2 (kafka.server.KafkaApis)
[2017-03-03 23:04:00,516] DEBUG principal = User:CN=kafkaclient2 is a super user, allowing operation without checking acls. (kafka.authorizer.logger)
[2017-03-03 23:04:00,516] DEBUG Principal = User:CN=kafkaclient2 is Allowed Operation = Describe from host = 172.18.0.3 on resource = Topic:test (kafka.authorizer.logger)
[2017-03-03 23:04:00,517] DEBUG principal = User:CN=kafkaclient2 is a super user, allowing operation without checking acls. (kafka.authorizer.logger)
[2017-03-03 23:04:00,517] DEBUG Principal = User:CN=kafkaclient2 is Allowed Operation = Write from host = 172.18.0.3 on resource = Topic:test (kafka.authorizer.logger)
```
Interesting, that works.  So it's either the echo or the separate container but in the same docker network.

So trying the same but adding the echo
```
	echo $(date) | bin/kafka-console-producer.sh ...
```
We see in the log that it seems successful again:

```
[2017-03-03 23:13:30,853] TRACE [KafkaApi-0] Handling request:{api_key=0,api_version=2,correlation_id=1,client_id=console-producer} -- {acks=1,timeout=1500,topic_data=[{topic=test,data=[{partition=0,record_set=java.nio.HeapByteBuffer[pos=0 lim=61 cap=61]}]}]} from connection 172.18.0.3:9093-172.18.0.3:40836;securityProtocol:SSL,principal:User:CN=kafkaclient2 (kafka.server.KafkaApis)
[2017-03-03 23:13:30,853] DEBUG principal = User:CN=kafkaclient2 is a super user, allowing operation without checking acls. (kafka.authorizer.logger)
[2017-03-03 23:13:30,853] DEBUG Principal = User:CN=kafkaclient2 is Allowed Operation = Describe from host = 172.18.0.3 on resource = Topic:test (kafka.authorizer.logger)
[2017-03-03 23:13:30,853] DEBUG principal = User:CN=kafkaclient2 is a super user, allowing operation without checking acls. (kafka.authorizer.logger)
[2017-03-03 23:13:30,853] DEBUG Principal = User:CN=kafkaclient2 is Allowed Operation = Write from host = 172.18.0.3 on resource = Topic:test (kafka.authorizer.logger)
[2
```
So, maybe it's cuz the producer is in a different container than the broker?

Checking again:
```
docker exec -it kafka bash -c "echo $(date) | bin/kafka-console-producer.sh --broker-list kafka:9093 --topic test --producer-property ssl.key.password=111111 --producer-property ssl.keystore.location=/var/private/ssl/client.2.keystore.jks --producer-property ssl.keystore.password=222222 --producer-property ssl.truststore.location=/var/private/ssl/client.truststore.jks --producer-property ssl.truststore.password=222222 --producer-property client.id=producer.2 --producer-property security.protocol=SSL"
```
triggers a successful log "super user, allowing" as before.

Just a passing thought, maybe improper line breaks due to a copy/paste issue on line wraps cause the failure.

Trying the separate container again:
```
docker run -it --rm --name kafkaproducer --net $NETWORK_NAME_KAFKA -v certs:$CERTS_DIR $IMAGE_KAFKA bash -c "echo $(date) | bin/kafka-console-producer.sh --broker-list kafka:9093 --topic test --producer-property ssl.key.password=111111 --producer-property ssl.keystore.location=/var/private/ssl/client.2.keystore.jks --producer-property ssl.keystore.password=222222 --producer-property ssl.truststore.location=/var/private/ssl/client.truststore.jks --producer-property ssl.truststore.password=222222 --producer-property client.id=producer.2 --producer-property security.protocol=SSL"
```
Again it seems to work, so it's not cuz it's a separate container.  So what's the diff with the script?


Here's from the script without the escaped new lines and replacing the local variables.  It, as before, does not connect as a superuser.
```
	docker run -it --rm --name kafkaproducer --net $NETWORK_NAME_KAFKA -v certs:$CERTS_DIR $IMAGE_KAFKA bash -c "echo $NOW_HUMAN | bin/kafka-console-producer.sh --broker-list kafka:9093 --topic $ZOO_TOPIC_NAME" --producer-property ssl.key.password=$KEY_PASS --producer-property ssl.keystore.location=$CERTS_DIR/client.2.keystore.jks --producer-property ssl.keystore.password=$STORE_PASS --producer-property ssl.truststore.location=$CERTS_DIR/client.truststore.jks --producer-property ssl.truststore.password=$STORE_PASS --producer-property client.id="producer.2" --producer-property security.protocol=SSL
```
```
javax.net.ssl.SSLException: Inbound closed before receiving peer's close_notify: possible truncation attack?
```	
The differences are:
- $(date) vs. $NOW_HUMAN
- test vs. $ZOO_TOPIC_NAME"
- some expected substitutions
- Ahh, the quotes are different; the quote in the script is too soon.

Fixed, so now
```
$ produceToTopicTest 1
Producer log: [2017-03-03 23:51:00,011] WARN Error while fetching metadata with correlation id 0 : {test=UNKNOWN_TOPIC_OR_PARTITION} (org.apache.kafka.clients.NetworkClient)
$ produceToTopicTest 2
Broker log: [2017-03-03 23:52:28,583] DEBUG principal = User:CN=kafkaclient2 is a super user, allowing operation without checking acls. (kafka.authorizer.logger)
```

----------------------

#### PROBLEM:
	docker run -it --rm  --net $NETWORK_NAME_KAFKA -v certs:$CERTS_DIR --env KEY_PASS="$KEY_PASS" $IMAGE_KAFKA timeout 2s openssl s_client -debug -connect kafka:9093 -tls1 -cert $CERTS_DIR/ca-cert -key $CERTS_DIR/ca-key

Returns output that includes:

	gethostbyname failure
	connect:errno=22

#### SOLUTION:
kafka not up

----------------------

#### PROBLEM:
	docker run -it --rm  --net $NETWORK_NAME_KAFKA -v certs:$CERTS_DIR --env KEY_PASS="$KEY_PASS" $IMAGE_KAFKA timeout 2s openssl s_client -debug -connect kafka:9093 -tls1 -cert $CERTS_DIR/ca-cert -key $CERTS_DIR/ca-key

Returns output that includes (ends with):

	verify error:num=19:self signed certificate in certificate chain

#### SOLUTION:
Maybe the reason for this is that the ca-cert is self signed.

----------------------

#### PROBLEM:
See this exception when client should be able to consume given a proper ACL.  But CID=0, a superuser, works.
```
	$ CID=1; docker run -it --rm --name kafkaconsumer --net $NETWORK_NAME_KAFKA -v certs:$CERTS_DIR $IMAGE_KAFKA bin/kafka-console-consumer.sh --bootstrap-server kafka:9093 --topic test --from-beginning --consumer-property security.protocol=SSL --consumer-property ssl.truststore.location=$CERTS_DIR/client.truststore.jks --consumer-property ssl.truststore.password=$STORE_PASS --consumer-property ssl.keystore.location=$CERTS_DIR/client.$CID.keystore.jks --consumer-property ssl.keystore.password=$STORE_PASS --consumer-property ssl.key.password=$KEY_PASS

	org.apache.kafka.common.errors.GroupAuthorizationException: Not authorized to access group: console-consumer-50630
```

#### SOLUTION:
	[Kafka Authorization Command Line Interface](https://cwiki.apache.org/confluence/display/KAFKA/Kafka+Authorization+Command+Line+Interface) 

		kafka-acls.sh ... --consumer-group <consumer-group>

Specifies the consumer-group as resource.  Option type is "Resource" (of Action, Configuration, Resource, Principal, Host, ...)

		kafka-acls.sh ...  --consumer

Convenience option to add/remove acls for consumer role. This will generate acls that allows READ,
DESCRIBE on topic and READ on consumer-group.

"--consumer" also needs --group="*".

----------------------

#### PROBLEM:
#### SOLUTION:

----------------------


Direction
---------
Registration service, maybe REST, which (draft):

- Itself has authentication for a namespace.
- Registers topic names for the namespace.  Also delete topic.  (Maybe don't allow access to zookeeper since it may not be secured with SSL? Act as a facade for it.)
- Provides the kafka proxy ip and port. (Internally implemented with network per namespace concept OR single service instance)
- Add Kafka clients to the namespace, either producer and/or consumer, as is done through the scripts.
- It returns a client truststore which contains the CA cert. Maybe returns a GET URL for each.
- On adding a client it returns created client certificates (or client provides cert?)
- Brokers also use this registration service to register new brokers to the CA.

Multi Kafka broker, multi Zookeeper with proxy.  Only need the single proxy when it is itself HA with floating IP, etc.  Use registration service above to register new broker / zookeeper instances.


TODO
----
- DONE the super.user~ line in config/server.properties
- *optional*: the log4j.properties debug settings in docker
- docker run --net kafkanet --restart=always --env ZOO_MY_ID=1 --env ZOO_SERVERS="server.1=zoo1:2888:3888 server.2=zoo2:2888:3888 server.3=zoo3:2888:3888" -d zookeeper
- unclean.leader.election.enable, to support use cases where downtime is preferable to inconsistency when all go down
- log compaction - only last saved
- "For broker/ZooKeeper communication, we will only require Kerberos authentication as TLS is only supported in ZooKeeper 3.5, which is still at the alpha release stage."  Hmm.
- Should the broker and client aliases be localhost or kafka or something else?
- Note: docker run -it --rm  --net $NETWORK_NAME_KAFKA -v certs:$CERTS_DIR --env KEY_PASS="$KEY_PASS" $IMAGE_KAFKA openssl s_client  -connect kafka:9093  -showcerts
- DONE use certs in consumer and producer
- DONE openssl cert w CN to authenticate
- DONE authorize to a topic
- kafka and zookeeper data to named volume(s)
- ###### verify ports to expose for consumer (run consumer outside of kafkanet)
- non root containers
- proxy to kafka and zookeeper; maybe zookeeper and, therefore create topic, would be hidden behind docker network
- DONE if SSL ACL cannot work, try SASL/kerberos
- ###### superuser broker cert so that brokers can talk to each other without ACLs; *** test *** it w/ multiple brokers.
- DONE add an ACL that prevents kafkaclient1
- DONE functions and parameterize with servernum and clientnum and cn
- DONE add a client whose cn is "asuperuser" in addition to "kafkaclient1" and be sure both clients can read the queue
- Make a certs-like volume for client and server and CA since some shouldn't have access
- consider file type volumes for kafka data and zookeeper data, maybe only one of the brokers and one of the zookeepers is necessary
- when a broker comes up create a cert and add to CA
- on a new client registration create a client cert and add to CA; include createTopic since it talks to zookeeper
- Could producer interceptor.classes be used to change the topic name if desired?
- Be sure that the client/customer registration script does not allow a user name for these super users: super.users=User:CN=kafkabroker;User:CN=kafkasuperclient
- ###### Test w multiple brokers. 1. --broker-list kafka1:9093,kafka2:9093 2. kafka:9093 SEE netknow-nginx.conf.  Test one down broker.
- ?limit who can create topics?  It is not SSL or authenticated when trying it.  It seems to know the kafka port (PLAINTEXT on 9092?) used via the provided zookeeper?
- Test Certificate passwords should be different for client(s) and server.  Maybe generate passwords.
- DONE produceToTopicTest on output test=UNKNOWN_TOPIC_OR_PARTITION (no ACL) quit after 10 tries (1s) so we can continue during a test that expects failure.
- DONE produceToTopicTest / consume ACL failure test
- Handle consumer group
- Test consumer multiple partitions 
- ###### Single Node-Multiple Brokers Configuration
- java consumer for better unit tests and experience
- use case of consumer with exception where the message remains in the topic (indexing) until processed successfully
- DONE Write test of two topics and two clients without access to each other.
- Method to register a new client which adds a new cert
- Method to register a new server which adds a new cert HMM, consider same cert for all servers?
- Write test proving auth works w multiple kafka broker, zoo, and client
- Write test for partitions and multi broker
- ###### Move scripts (tests included) into docker image(s).
- add ${keystoreFilePrefix} to --override broker.id=${keystoreFileIndex}
- This may or may not be necessary depending on if we have a shared volume among all brokers. --override log.dirs=/tmp/kafka-logs-$keystoreFileIndex
- DONE check which broker is listening on the topic for unit testing. (PartitionCount:1 ReplicationFactor:3...)
- DONE superuser auth
- DONE ACL auth on producer
- multi zookeeper
- kafka config auto.leader.rebalance.enable=true; consider broker.rack=my-rack-id
- datacenters / latency


QUESTIONS
---------
- Does Zookeeper need encryption?  That is, are the Zookeepers on an internal network?  Kafka would not need to be encrypted internally as well it seems.  Zookeeper, in my understanding, cannot use SSL but could use SASL/kerberos which we may find unnecessary.
- Does an existing customer use auto.create.topics.enable?  We may want that as a separate step in registration?  To think about...
- Is there a need to give client producers and consumers and topic creators (done via registration) different ACLs?  Producers READ and WRITE on the topic resource.  Consumers READ on the topic resource and READ on the consumer-group resource.  To create a topic CREATE and DESCRIBE on the topic resource via the Metadata API with auto.create.topics.enable.
- Thinking of client, topic, broker registration methods (REST?).  Receive? or provide? for them certs which registration adds to our CA which is provided.
